---
title: Pitch Presentation
description: Pitch presentation during WCS reunion 2022
author: Orwa M. Diraneyya (@odiraneyya)
keywords: marp,marp-cli,slide
url: 
marp: true
image: ./assets/logo.png
---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

![](./assets/logo.png){.float-right}
# Pitch Presentation | Selling Student Data
## Orwa, Olga, Liya, David, Alexander, Cecile

> Sell or exchange student data for access to external and well-established asynchronous learning platforms

---

# Organization

- Mind-map exploration of concept 🌐
- Regions of interest 🤖
- Possible business scheme and target 🚀

---

# 1 Mind-map exploration

> The _root concept_ for our mindmap exploration is the **intangible asset**, which is a bigger concept including all forms of data created by our students throughout their bootcamp journey.

::: fence

@startuml

@startmindmap

*[#LightBlue] Intangible assets
**[#Orange] Student experimentation interactions
**[#Orange] Student peer reviews of other students
** Student social interaction (surveys)
** Student education history
** Project work
*** Research
*** Code
**[#Yellow] Tokenized (copyrighted) student work
***[#Yellow] NFT
*** ...
** ...

@endmindmap

@enduml

:::


---

# 2.1 Region of interest

Specific parts of the previous exploration caught our interest:

- Student experimentation interactions and generated content 
- Student peer reviews of other students

> These areas are interesting because they are currently generated and thrown away. Turning these into value, thus and by definition, **is a step towards a 100% growth.**

---

# 2.2 Region of interest

Another area of interest is selling tokenized student work (e.g. as an NFT).

Non-Fungible-Tokens or NFTS are a novel method of using the blockchain to attach novelty or copyright value to material, in this case, student computer code. Thanks to a NFT technology, the data is hashed by cryptography (can't be falsified, infringed)

---

# 3 Possible business plan

> KATACODA is an asynchronous learning platform that was acquired by O'REILLY, the publishing company.

![](./assets/katacoda.png)

---

# 4 How this might work

> By granting KATACODA (Now O'Reilly Online Learning) access to _additional_ anonymized student learning data, we can either get compensated monetarily or gain access to the platform's asynchronous solutions for free.

The same strategy can be applied to gain partnership with other platform providers.

---

# 5 Main potential issue

The main issue with this concept the possible inhibition or backing off from the bootcamp for reasons of data privacy (should the students know their data is being shared with third parties).

Starting from anonymizing the data, we can come up with various arguments that highlights the benefits of this sharing which should be sufficient to _reduce the percentage of people to whom this might be a deal beaker to an insignificant proportion_ (from a sales perspective).

---

# 6 Necessary steps

- Data collection 
- Integration into Odyssey 
- Partnerships with other companies 
- Analysis of the result 

Team: Sales, Partnership Managers, Data Analysts, Developers 